# Sonarqube
## Setup

### 1. สร้าง sonarqube server บน docker
```
$ docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube
```
### เมื่อติดตั้งเรียบร้อยเข้าใช้งานที่
```
http://localhost:9000
```

[![Screen-Shot-2563-12-28-at-11-15-31.png](https://i.postimg.cc/ncpNJpm9/Screen-Shot-2563-12-28-at-11-15-31.png)](https://postimg.cc/4n2WvCBX)

 *สามารถ Login โดยใช้ username password ว่า admin/admin เมื่อ login แล้วสามารถเปลี่ยน password ได้

 ### 2. เมื่อ login สำเร็จก็สามารถทำการสร้าง Project ขึ้น โดยกดที่ปุ่ม Create new project

[![1-X5z-COU1nrf-UD-zwl6-IKATQ.png](https://i.postimg.cc/JnfvXsR7/1-X5z-COU1nrf-UD-zwl6-IKATQ.png)](https://postimg.cc/S2GgFNXH)


### 3.  ตั้งชื่อ Project ว่า "example"
[![Screen-Shot-2563-12-28-at-11-17-22.png](https://i.postimg.cc/KvGpWQXn/Screen-Shot-2563-12-28-at-11-17-22.png)](https://postimg.cc/4n0176xd)

### 4. เมื่อสร้าง Project แล้ว เราต้องสร้าง token เพื่อที่จะใช้ในการอัพโหลดผลการ scan ขึ้นมา โดยจะกดปุ่ม Generate

[![Screen-Shot-2563-12-28-at-11-17-45.png](https://i.postimg.cc/rmTNrFMS/Screen-Shot-2563-12-28-at-11-17-45.png)](https://postimg.cc/Tykb8xyw)

### 5. เมื่อเราได้ token มาแล้ว ให้เก็บเอาไว้ก่อน

[![Screen-Shot-2563-12-28-at-11-18-03.png](https://i.postimg.cc/4dFnTBLm/Screen-Shot-2563-12-28-at-11-18-03.png)](https://postimg.cc/hzTKLbqB)

จากนั้นเลือกประเภท Project เป็น C# ตัว SonarQube จะทำ sample script มาให้ ซึ่งมันจะใช้ MSBUILD

[![Screen-Shot-2563-12-28-at-11-18-31.png](https://i.postimg.cc/FRcF2NLn/Screen-Shot-2563-12-28-at-11-18-31.png)](https://postimg.cc/FdrQStGj)


### 6. ติดตั้ง SonarQube Scanner
```
dotnet tool install --global dotnet-sonarscanner
```
### 7. ติดตั้ง coverlet console
```
dotnet tool install --global coverlet.console
```

#
# Set Sonarqube + .net core project
## สร้างโปรเจ็ค example

### 1. New Empty project ตั้งชื่อว่า example

[![Screen-Shot-2563-12-28-at-14-23-37.png](https://i.postimg.cc/8zDMsZGj/Screen-Shot-2563-12-28-at-14-23-37.png)](https://postimg.cc/mtXt5Nts)

#### 1.1. สร้างฟังก์ชั่นตัวอย่างที่ใช้ในการทดสอบ ไว้ใน controllers/AreaCalculator.cs

```
namespace example.controllers
{
    public class AreaCalculator
    {
        public AreaCalculator()
        {}
        public double rectangle(double Length, double Width) {
            if (Length > 0 && Width > 0)
            {
                return Length * Width;
            }
            else {
                return 0;
            }

        }

        public double Circle(double r )
        {
            var PI = 3.1415926535897931;
            if (r > 0)
            {
                return Math.Round(PI * (r * r), 2, MidpointRounding.AwayFromZero);
            }
            else {
                return 0.0;
            }

        }
    }
}
```
### 2. เพิ่ม โปรเจ็ค test เช้าไปยัง solution

[![Screen-Shot-2563-12-28-at-09-22-24.png](https://i.postimg.cc/zDCPQPxf/Screen-Shot-2563-12-28-at-09-22-24.png)](https://postimg.cc/9zfpR153)

### 2.1. ในตัวอย่างนี้เลือกโปรเจ็ค test แบบ xUnit Test Project
[![Screen-Shot-2563-12-28-at-09-22-37.png](https://i.postimg.cc/RZd6hfvm/Screen-Shot-2563-12-28-at-09-22-37.png)](https://postimg.cc/21q8X1q9)

### 2.2. ตั้งชื่อว่า test

[![Screen-Shot-2563-12-28-at-09-23-27.png](https://i.postimg.cc/85BBZxTQ/Screen-Shot-2563-12-28-at-09-23-27.png)](https://postimg.cc/47y9dLKB)

### 2.1. Add project reference ที่ต้องการให้กับ test project

[![Screen-Shot-2563-12-28-at-10-35-16.png](https://i.postimg.cc/kGHcSKnB/Screen-Shot-2563-12-28-at-10-35-16.png)](https://postimg.cc/vxWfF1xs)
### 2.2. สร้างตัวอย่าง unit test

```
using System;
using example.controllers;
using Xunit;

namespace test
{
    public class UnitTest1
    {
        [Fact]
        public void rectangleAreaTest()
        {
            AreaCalculator cal = new AreaCalculator();
            var res = cal.rectangle(5, 3);

            Assert.Equal(15, res);
        }

        [Fact]
        public void rectangleZeroAreaTest()
        {
            AreaCalculator cal = new AreaCalculator();
            var res = cal.rectangle(0,5);

            Assert.Equal(0, res);
        }

    }
}

```
### 2.3. Install packet เข้าไปที่ Project Test
```
dotnet add package coverlet.msbuild
dotnet add package coverlet.collector
```
หรือจะติดตั้งผ่าน NuGet
- coverlet.msbuild

[![Screen-Shot-2563-12-28-at-10-44-04.png](https://i.postimg.cc/dtdHCNyF/Screen-Shot-2563-12-28-at-10-44-04.png)](https://postimg.cc/qzJxVLwD)

- coverlet.collector

[![Screen-Shot-2563-12-28-at-10-45-42.png](https://i.postimg.cc/wvmJrZHT/Screen-Shot-2563-12-28-at-10-45-42.png)](https://postimg.cc/xJ9cJscW)


#
## Script Sonarqube Scan

### 1. restore and build solution
```
$ dotnet restore example.sln
$ dotnet build example.sln
```
### 2. run test
```
$ dotnet test test/test.csproj /p:CollectCoverage=true /p:CoverletOutputFormat=opencover
```
** "test/test.csproj" คือ path project ที่ต้องการ run test

### เมื่อรัน test จะได้ผลลัพธ์ที่ ไฟล์ coverage.opencover.xml
```
Calculating coverage result...
  Generating report '/Users/theerapong-srs/Works/ETDA E-office2/source code/sonarqube/example/test/coverage.opencover.xml'

+---------+--------+--------+--------+
| Module  | Line   | Branch | Method |
+---------+--------+--------+--------+
| example | 36.58% | 62.5%  | 42.85% |
+---------+--------+--------+--------+

+---------+--------+--------+--------+
|         | Line   | Branch | Method |
+---------+--------+--------+--------+
| Total   | 36.58% | 62.5%  | 42.85% |
+---------+--------+--------+--------+
| Average | 36.58% | 62.5%  | 42.85% |
+---------+--------+--------+--------+

```
### 3. build-server shutdown
```
$ dotnet build-server shutdown
```
### 4. สั่งให้ sonarqube scan
```
$ dotnet sonarscanner begin /k:"example" /d:sonar.host.url="http://localhost:9000" /d:sonar.login="bee1e1811929a6267d0b08da836fea8edd061ca6" /d:sonar.language="cs" /d:sonar.cs.opencover.reportsPaths="test/coverage.opencover.xml"
```
#### 5. สั่ง build solution อีกครั้ง
```
$ dotnet build example.sln
```
### 6. สั่งสิ้นสุดการ scan แล้วนำผลลัพธ์แสดงผลบน sonaqube
```

$ dotnet sonarscanner end /d:sonar.login="bee1e1811929a6267d0b08da836fea8edd061ca6"
```
### * 1. /k:"example"  ใส่ ชื่อ project ที่ตั้ง
### ** 2. "bee1e1811929a6267d0b08da836fea8edd061ca6"  ตือ token ที่ได้ทำการ generate ไว้

### **** หลังจาก run script เรียบร้อย สามารถดูผลได้ที่ http://localhost:9000

[![Screen-Shot-2563-12-28-at-15-23-57.png](https://i.postimg.cc/G20MpZc6/Screen-Shot-2563-12-28-at-15-23-57.png)](https://postimg.cc/jWX6g3nh)
