﻿using System;
namespace example.controllers
{
    public class AreaCalculator
    {
        public AreaCalculator()
        {}
        public double rectangle(double Length, double Width) {
            if (Length > 0 && Width > 0)
            {
                return Length * Width;
            }
            else {
                return 0;
            }
            
        }

        public double Circle(double r )
        {
            var PI = 3.1415926535897931;
            if (r > 0)
            {
                return Math.Round(PI * (r * r), 2, MidpointRounding.AwayFromZero);
            }
            else {
                return 0.0;
            }
            
        }
    }
}
