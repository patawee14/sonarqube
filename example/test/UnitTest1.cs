using System;
using example.controllers;
using Xunit;

namespace test
{
    public class UnitTest1
    {
        [Fact]
        public void rectangleAreaTest()
        {
            AreaCalculator cal = new AreaCalculator();
            var res = cal.rectangle(5, 3);

            Assert.Equal(15, res);
        }

        [Fact]
        public void rectangleZeroAreaTest()
        {
            AreaCalculator cal = new AreaCalculator();
            var res = cal.rectangle(0,5);

            Assert.Equal(0, res);
        }

    }
}
